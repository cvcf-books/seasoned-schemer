;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; The Little Schemer

(define (atom? x)
  (and (not (pair? x)) (not (null? x))))

(define (member? x xs)
  (cond ((null? xs) #f)
        (else (or (eq? x (car xs))
                  (member? x (cdr xs))))))

(define (add1 x)
  (+ x 1))

(define (sub1 x)
  (- x 1))

;; NB: We called this add in the exercises for The Little Schemer
(define (o+ x y)
  (cond ((zero? x) y)
        (else (o+ (sub1 x) (add1 y)))))

(define (one? x)
  (and (number? x) (eq? x 1)))

(define (pick n xs)
  (cond ((one? n) (car xs))
        (else (pick (sub1 n) (cdr xs)))))

(define (rember-f test?)
  (lambda (x xs)
    (cond ((null? xs) '())
          ((test? (car xs) x) (cdr xs))
          (else (cons (car x) ((rember-f test?) x (cdr xs)))))))

(define rember-eq? (rember-f eq?))

(define (makeset xs)
  (cond ((null? xs) '())
        (else (cons (car xs)
                    (makeset (multirember-eq? (car xs) (cdr xs)))))))

(define (eqlist? xs ys)
  (cond ((and (null? xs) (null? ys)) #t)
        ((or  (null? xs) (null? ys)) #f)
        ((and (atom? (car xs)) (atom? (car ys)))
         (and (eq? (car xs) (car ys)) (eqlist? (cdr xs) (cdr ys))))
        ((or  (atom? (car xs)) (atom? (car ys))) #f)
        (else (and (eqlist? (car xs) (car ys)) (eqlist? (cdr xs) (cdr ys))))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Chapter 11

;;;;
;; Exercise 01

(define (is-first? x xs)
  (cond ((null? xs) #f)
        (else (eq? x (car xs)))))

(define (is-first-b? x xs)
  (cond ((null? xs) #f)
        (else (or (eq? x (car xs))
                  (two-in-a-row? xs)))))

(define (two-in-a-row? xs)
  (cond ((null? xs) #f)
        (else (two-in-a-row-b? (car xs) (cdr xs)))))

(define (two-in-a-row-b? x xs)
  (cond ((null? xs) #f)
        (else (or (eq? x (car xs))
                  (two-in-a-row-b? (car xs) (cdr xs))))))

;;;;
;; Exercise 02

(define (sum-of-prefixes tup)
  (cond ((null? tup) '())
        (else (cons (car tup) (sum-of-prefixes-b (car tup) (cdr tup))))))

(define (sum-of-prefixes-b sum tup)
  (cond ((null? tup) '())
        (else (cons (o+ sum (car tup))
                    (sum-of-prefixes-b (o+ sum (car tup)) (cdr tup))))))

;;;;
;; Exercise 03

(define (scramble-b tup rev-pre)
  (cond ((null? tup) '())
        (else (cons (pick (car tup) (cons (car tup) rev-pre))
                    (scramble-b (cdr tup) (cons (car tup) rev-pre))))))

(define (scramble tup)
  (scramble-b tup '()))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Chapter 12

;;;;
;; Exercise 01

(define (multirember-f test?)
  (lambda (x xs)
    (letrec ((mr
              (lambda (xs)
                (cond ((null? xs) '())
                      ((test? x (car xs)) ((multirember-f test?) x (cdr xs)))
                      (else (cons (car xs) ((multirember-f test?) x (cdr xs))))))))
      (mr xs))))

(define multirember-eq? (multirember-f eq?))

;;;;
;; Exercise 02

;; multirember-f accepts a test function that can compare two items and returns
;; a function that uses the provided testing function to remove items that match
;; a specified item from the list.

;;;;
;; Exercise 03

(define (union s1 s2)
  (letrec ((rec (lambda (s)
                  (cond ((null? s) s2)
                        ((member? (car s) s2) (rec (cdr s)))
                        (else (cons (car s) (rec (cdr s))))))))
    (rec s1)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Chapter 13

;;;;
;; Exercise 01

(define (intersect s1 s2)
  (letrec ((rec (lambda (s)
                  (cond ((null? s) '())
                        ((member? (car s) s2) (cons (car s) (rec (cdr s))))
                        (else (rec (cdr s)))))))
    (rec s1)))

;;;;
;; Exercise 02

(define (rember x xs)
  (letrec ((rec (lambda (xs)
                  (cond ((null? xs) '())
                        ((eq? (car xs) x) (cdr xs))
                        (else (cons (car xs) (rec (cdr xs))))))))
    (rec xs)))

;;;;
;; Exercise 03

;; rember-beyond-first returns a list of all elements up to x, if it is
;; contained in the list; and if x is not in the list, then it returns the
;; original list.

;;;;
;; Exercise 04

;; rember-upto-last returns a list of all elements after the last x contained in
;; the list. If x is not contained in the list, then it returns the original
;; list.

;;;;
;; Exercise 05

;; NB: Guile and Chez don't recognize letcc or let/cc - use Racket
(define (rember-upto-last x xs)
  (let/cc skip
    (letrec ((rec (lambda (xs)
                    (cond ((null? xs) '())
                          ((eq? (car xs) x) (skip (rec (cdr xs))))
                          (else (cons (car xs) (rec (cdr xs))))))))
      (rec xs))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Chapter 14

;;;;
;; Exercise 01

(define (leftmost xs)
  (cond ((null? xs) '())
        ((atom? (car xs)) (car xs))
        (else (let ((x (leftmost (car xs))))
                (cond ((atom? x) x)
                      (else (leftmost (cdr xs))))))))

;;;;
;; Exercises 02 and 03

(define (rember1* x xs)
  (letrec ((rec (lambda (xs)
                  (cond ((null? xs) '())
                        ((atom? (car xs))
                         (cond ((eq? (car xs) x) (cdr xs))
                               (else (cons (car xs) (rec (cdr xs))))))
                        (else
                         (let ((ys (rec (car xs))))
                           (cond ((eqlist? ys (car xs)) (cons (car xs) (rec (cdr xs))))
                                 (else (cons ys (cdr xs))))))))))
    (rec xs)))

;;;;
;; Exercise 04

(define (depth* xs)
  (cond ((null? xs) 1)
        ((atom? (car xs)) (depth* (cdr xs)))
        (else (let ((d1 (add1 (depth* (car xs))))
                    (d2 (depth* (cdr xs))))
                (if (> d2 d1) d2 d1)))))

;;;;
;; Exercises 05 and 06

(define (depth* xs)
  (cond ((null? xs) 1)
        (else (let ((cdr-depth (depth* (cdr xs))))
                (cond ((atom? (car xs)) cdr-depth)
                      (else (let ((car-depth (add1 (depth* (car xs)))))
                              (if (> cdr-depth car-depth) cdr-depth car-depth))))))))

;;;;
;; Exercise 07

;; The version from Exercise 04 is most readable

;;;;
;; Exercise 08

(define (max x y)
  (if (> x y) x y))

(define (depth* xs)
  (cond ((null? xs) 1)
        ((atom? (car xs)) (depth* (cdr xs)))
        (else (max (add1 (depth* (car xs))) (depth* (cdr xs))))))

;;;;
;; Exercise 09

(define (scramble tup)
  (letrec ((rec (lambda (tup rp)
                  (cond ((null? tup) '())
                        (else (let ((rp (cons (car tup) rp)))
                                (cons (pick (car tup) rp)
                                      (rec (cdr tup) rp))))))))
    (rec tup '())))

;;;;
;; Exercises 10 and 11

(define (leftmost xs)
  (let/cc skip
    (letrec ((rec (lambda (xs)
                    (cond ((null? xs) '())
                          ((atom? (car xs)) (skip (car xs)))
                          (else (begin
                                  (rec (car xs))
                                  (rec (cdr xs))))))))
      (rec xs))))

;;;;
;; Exercise 12

;; The new version of leftmost recurses through a list of atoms and sub-lists
;; searching for the leftmost atom by checking whether (car xs) is NIL and, if
;; it is, then there is no leftmost atom. If (car xs) is an atom, we break out
;; of the recursion since we've found the left-most atom, by this point.
;; Finally, if neither of the previous conditions are true, then we know that
;; both (car xs) and (cdr xs) are lists and so we search through each of them
;; for the left-most atom using the previously defined conditions.

;;;;
;; Exercise 13

(define (rm x xs oh)
  (cond ((null? xs) (oh 'no))
        ((atom? (car xs)) (if (eq? (car xs) x)
                              (cdr xs)
                              (cons (car xs) (rm x (cdr xs) oh))))
        (else (if (atom? (let/cc oh (rm x (car xs) oh)))
                  (cons (car xs) (rm x (cdr xs) oh))
                  (cons (rm x (car xs) 0) (cdr xs))))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Chapter 15

;;;;
;; Exercise 01

(define x '())

(define (dinerR food)
  (set! x food)
  (list 'milkshake food))

;;;;
;; Exercise 02

(define (chez-nous)
  (let ((tmp food))
    (set! food x)
    (set! x tmp)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Chapter 16

;;;;
;; Exercise 01

(define last 'angelfood)

(define (sweet-toothL food)
  (set! last food)
  (list food 'cake))

;;;;
;; Exercise 02

(define ingredients '())

(define (sweet-toothR food)
  (set! ingredients (cons food ingredients))
  (list food 'cake))

;;;;
;; Exercise 03

(define rs '())
(define ns '())

(define (deep n)
  (cond ((zero? n) 'pizza)
        (else (list (deep (sub1 n))))))

(define (deepR n)
  (let ((r (deep n)))
    (set! ns (cons n ns))
    (set! rs (cons r rs))
    r))

;;;;
;; Exercise 04

(define (find n ns rs)
  (letrec ((rec (lambda (ns rs)
                  (cond ((null? ns) #f)
                        ((= (car ns) n) (car rs))
                        (else (rec (cdr ns) (cdr rs)))))))
    (rec ns rs)))

;;;;
;; Exercise 05 and 06

(define (deepM n)
  (let ((x (find n ns rs)))
    (if (null? x)
        (let ((r (deep n)))
          (set! ns (cons n ns))
          (set! rs (cons r rs))
          r)
        x)))

;;;;
;; Exercise 07

(define (deep n)
  (cond ((zero? n) 'pizza)
        (else (list (deepM (sub1 n))))))

;;;;
;; Exercise 08

;; (define length
;;   (let ((h (lambda (l) 0)))
;;     (set! h x)
;;     h))
;; where x is whatever recursive function of 1 argument that you want.

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Chapter 17

;;;;
;; Exercise 01

(define deepM
  (let ((ns '())
        (rs '()))
    (lambda (n)
      (let ((exists (find n ns rs)))
        (if exists
            exists
            (let ((r (if (zero? n) 'pizza (consC (deepM (sub1 n)) '()))))
              (set! rs (cons r rs))
              (set! ns (cons n ns))
              r))))))

;;;;
;; Exercise 02

(define counter 0)
(define set-counter 0)

(define consC
  (let ((n 0))
    (set! counter (lambda () n))
    (set! set-counter (lambda (m) (set! n m)))
    (lambda (x y)
      (set! n (add1 n))
      (cons x y))))

(define (rember1*C x xs)
  (letrec ((rec (lambda (xs oh)
                  (cond ((null? xs) (oh 'no))
                        ((atom? (car xs)) (if (eq? (car xs) x)
                                              (cdr l)
                                              (consC (car xs) (rec (cdr xs) oh))))
                        (else (let ((new-car (let/cc oh (rec (car xs) oh))))
                                (if (atom? new-car)
                                    (consC (car xs) (rec (cdr xs) oh))
                                    (consC new-car (cdr xs)))))))))
    (let ((new-xs (let/cc oh (rec xs oh))))
      (if (atom? new-xs) xs new-xs))))

;;;;
;; Exercise 03

(define (rember1*C2 x xs)
  (letrec ((rec (lambda (xs)
                  (cond ((null? xs) '())
                        ((atom? (car xs)) (if (eq? (car xs) x)
                                              (cdr xs)
                                              (consC (car xs) (rec (cdr xs)))))
                        (else (let ((y (rec (car xs))))
                                (if (eqlist? (car xs) y)
                                    (consC (car xs) (rec (cdr xs)))
                                    (consC y (cdr xs)))))))))
    (rec xs)))
